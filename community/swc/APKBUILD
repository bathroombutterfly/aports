# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swc
pkgver=1.3.97
pkgrel=0
pkgdesc="A super-fast TypeScript / JavaScript compiler written in Rust"
url="https://swc.rs"
# riscv64: it would take eternity to build
arch="all !riscv64"
license="Apache-2.0"
makedepends="cargo cargo-auditable"
# tag disappeared - uncomment on upgrade
# source="https://github.com/swc-project/swc/archive/v$pkgver/swc-$pkgver.tar.gz"
source="https://dev.alpinelinux.org/archive/swc/swc-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/bindings"
# !check: TODO: run tests
# net: fetch dependencies
options="!check net"

prepare() {
	default_prepare

	# This is unwanted and breaks build on ARM.
	rm ../.cargo/config.toml

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build -p swc_cli --release --locked
}

package() {
	install -D -m755 target/release/swc -t "$pkgdir"/usr/bin/
}

sha512sums="
b6818ede0767ba7a89888b911a895da7c3d1ae83d24d329217c850510e26839656f17c5486b848b447f1b2b0d3cda2d45fdde0bebef4e02fecc93c8499e4c593  swc-1.3.97.tar.gz
"
